﻿using System;
using System.Collections.Generic;

using Adder.Adders;

namespace Adder
{
	public static class IntAdder
	{
		public static int Sum(IEnumerable<int> data, AddType type = AddType.Default)
		{
			return GetAdder(type).Sum(data);
		}

		private static IAdder GetAdder(AddType type)
		{
			return type switch
			{
				AddType.Default => new DefaultAdder(),
				AddType.Linq => new LinqAdder(),
				AddType.Plinq => new PlinqAdder(),
				AddType.Parallel => new ParallelAdder(),
				_ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
			};
		}
	}
}