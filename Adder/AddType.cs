﻿namespace Adder
{
	public enum AddType
	{
		Default,
		Linq,
		Plinq,
		Parallel
	}
}