﻿using System.Collections.Generic;
using System.Linq;

namespace Adder.Adders
{
	public class LinqAdder : IAdder
	{
		public int Sum(IEnumerable<int> data)
		{
			return data.Sum();
		}
	}
}