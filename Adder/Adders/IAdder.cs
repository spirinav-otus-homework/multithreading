﻿using System.Collections.Generic;

namespace Adder.Adders
{
	public interface IAdder
	{
		public int Sum(IEnumerable<int> data);
	}
}