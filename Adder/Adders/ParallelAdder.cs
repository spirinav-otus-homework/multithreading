﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Adder.Adders
{
	public class ParallelAdder : IAdder
	{
		private int _result;
		private readonly object _lock = new();

		public int Sum(IEnumerable<int> data)
		{
			var manualResetEvents = new List<ManualResetEvent>();

			var parts = Split(data, Environment.ProcessorCount);

			foreach (var p in parts)
			{
				var mre = new ManualResetEvent(false);
				manualResetEvents.Add(mre);

				new Thread(Calc).Start(new Tuple<IEnumerable<int>, ManualResetEvent>(p, mre));
			}

			WaitHandle.WaitAll(manualResetEvents.ToArray());

			return _result;
		}

		private IEnumerable<IEnumerable<int>> Split(IEnumerable<int> data, int partCount)
		{
			var partSize = (int) Math.Ceiling((double) data.Count() / partCount);
			while (data.Any())
			{
				yield return data.Take(partSize);
				data = data.Skip(partSize);
			}
		}

		private void Calc(object data)
		{
			if (data is not Tuple<IEnumerable<int>, ManualResetEvent> tuple)
			{
				return;
			}

			lock (_lock)
			{
				_result += tuple.Item1.Sum();
			}

			tuple.Item2.Set();
		}
	}
}