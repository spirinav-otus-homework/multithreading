﻿using System.Collections.Generic;

namespace Adder.Adders
{
	public class DefaultAdder : IAdder
	{
		public int Sum(IEnumerable<int> data)
		{
			var result = 0;
			foreach (var d in data)
			{
				result += d;
			}

			return result;
		}
	}
}