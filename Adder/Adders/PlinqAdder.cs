﻿using System.Collections.Generic;
using System.Linq;

namespace Adder.Adders
{
	public class PlinqAdder : IAdder
	{
		public int Sum(IEnumerable<int> data)
		{
			return data.AsParallel().Sum();
		}
	}
}