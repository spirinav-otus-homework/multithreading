﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Adder;

namespace ConsoleApp
{
	class Program
	{
		static void Main()
		{
			var arrayLengths = new[] {100_000, 1_000_000, 10_000_000};
			foreach (var length in arrayLengths)
			{
				Console.WriteLine(length);
				ExecuteAllAdders(Enumerable.Repeat(1, length));
			}

			Console.ReadLine();
		}

		private static void ExecuteAllAdders(IEnumerable<int> array)
		{
			var stopwatch = new Stopwatch();
			foreach (AddType type in Enum.GetValues(typeof(AddType)))
			{
				stopwatch.Start();
				IntAdder.Sum(array, type);
				stopwatch.Stop();

				Console.WriteLine($"{type} (ms):\t{stopwatch.ElapsedMilliseconds}");
			}
		}
	}
}